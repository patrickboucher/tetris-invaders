note
	description: "Tests unitaires de la classe {SCENE_OBJECT}."
	author: "Patrick Boucher"
	date: "5 novembre 2020"
	revision: "1.0"
	testing: "type/manual"

class
	SCENE_OBJECT_TESTS

inherit
	EQA_TEST_SET

feature -- Tests cas normaux

	test_make_normal
			-- Teste le constructeur `make'.
		note
			testing: "covers/{SCENE_OBJECT}.make"
		local
			scene_object: MOCK_SCENE_OBJECT
		do
			create scene_object.make
			assert ("local_position par d�faut", scene_object.local_position ~ {VECTOR}.zero)
			assert ("internal_children par d�faut", scene_object.children.is_empty)
		end

	test_set_local_position_normal
			-- Teste `set_local_position'.
		note
			testing:
				"covers/{SCENE_OBJECT}.local_position",
				"covers/{SCENE_OBJECT}.set_local_position"
		local
			scene_object: MOCK_SCENE_OBJECT
		do
			create scene_object.make
			assert ("local_position par d�faut", scene_object.local_position ~ {VECTOR}.zero)
			scene_object.set_local_position (2, 5)
			assert ("set_local_position x valide", scene_object.local_position.x ~ 2)
			assert ("set_local_position y valide", scene_object.local_position.y ~ 5)
		end

	test_set_local_position_with_vector_normal
			-- Teste `set_local_position_with_vector'.
		note
			testing:
				"covers/{SCENE_OBJECT}.local_position",
				"covers/{SCENE_OBJECT}.set_local_position_with_vector"
		local
			scene_object: MOCK_SCENE_OBJECT
			vecteur: VECTOR
		do
			create scene_object.make
			create vecteur.make_with_coordinates (5, 1)
			assert ("local_position par d�faut", scene_object.local_position ~ create {VECTOR})
			scene_object.set_local_position_with_vector (vecteur)
			assert ("set_local_position_with_vector x valide", scene_object.local_position.x ~ 5)
			assert ("set_local_position_with_vector y valide", scene_object.local_position.y ~ 1)
		end

	test_scene_position_normal
			-- Teste `scene_position' avec et sans parent.
			-- `scene_position' correspond � la position du parent dans la sc�ne + la position de
			-- l'objet par rapport � son parent.
			-- Si l'objet n'a pas de parent, `scene_position' correspond � `local_position'.
		note
			testing: "covers/{SCENE_OBJECT}.scene_position"
		local
			scene_object, parent: MOCK_SCENE_OBJECT
		do
			create scene_object.make
			create parent.make
			assert ("scene_position par d�faut", scene_object.scene_position ~ {VECTOR}.zero)
			scene_object.set_local_position (1, 3)
			assert ("scene_position without parent x valide", scene_object.scene_position.x ~ 1)
			assert ("scene_position without parent y valide", scene_object.scene_position.y ~ 3)
			parent.set_local_position (10, 10)
			parent.add_child (scene_object)
			assert ("scene_position with parent x valide", scene_object.scene_position.x ~ 11)
			assert ("scene_position with parent y valide", scene_object.scene_position.y ~ 13)
		end

	test_set_scene_position_normal
			-- Teste `set_scene_position' avec et sans parent.
		note
			testing:
				"covers/{SCENE_OBJECT}.scene_position",
				"covers/{SCENE_OBJECT}.set_scene_position",
				"covers/{SCENE_OBJECT}.set_scene_position_with_vector"
		local
			scene_object, parent: MOCK_SCENE_OBJECT
		do
			create scene_object.make
			create parent.make
			scene_object.set_scene_position (5, 8)
			assert ("scene_position sans parent x valide", scene_object.scene_position.x ~ 5)
			assert ("scene_position sans parent y valide", scene_object.scene_position.y ~ 8)
			parent.set_local_position (10, 10)
			parent.add_child (scene_object)
			scene_object.set_scene_position (-5, 3)
			assert ("scene_position avec parent x valide", scene_object.scene_position.x ~ -5)
			assert ("scene_position avec parent y valide", scene_object.scene_position.y ~ 3)
		end

	test_children_normal
			-- Teste `children'.
			-- Doit retourner un copie de `internal_children'.
		note
			testing: "covers/{SCENE_OBJECT}.children"
		local
			l_scene_object, l_child: MOCK_SCENE_OBJECT
			l_copy: LIST [SCENE_OBJECT]
			l_index: INTEGER_32
		do
			create l_child.make
			create l_scene_object.make
			l_scene_object.add_child (l_child)
			l_copy := l_scene_object.children
			if l_copy.count ~ l_scene_object.internal_children.count then
				from
					l_index := 1
				until
					l_index > l_copy.count
				loop
					assert ("children identique � internal_object",
					l_copy.at (l_index) = l_scene_object.internal_children.at (l_index))
					l_index := l_index + 1
				end
			else
				assert ("erreur: les listes ne sont pas de m�me taille", False)
			end
			l_copy.wipe_out
			l_copy := l_scene_object.children
			assert ("est une copie", l_copy.count ~ 1)
		end

	test_add_child_normal
			-- Teste `add_child'.
			-- Une relation parent-enfant doit �tre cr��e.
			-- Un changement de parent implique que l'ancien parent ne doit plus contenir l'enfant.
		note
			testing:
				"covers/{SCENE_OBJECT}.children",
				"covers/{SCENE_OBJECT}.add_child",
				"covers/{SCENE_OBJECT}.set_parent"
		local
			l_child, l_new_parent, l_worthless_parent: MOCK_SCENE_OBJECT
		do
			create l_worthless_parent.make
			create l_child.make
			l_worthless_parent.add_child (l_child)
			assert ("lien enfant-parent valide",
				l_child.parent = l_worthless_parent and l_worthless_parent.has_child (l_child))
			create l_new_parent.make
			l_new_parent.add_child (l_child)
			assert ("lien enfant-parent (nouveau parent) valide",
				l_child.parent = l_new_parent and l_new_parent.has_child (l_child))
			assert ("lien rompu", not l_worthless_parent.has_child (l_child))
		end

	test_remove_child_normal
			-- Teste `remove_child'.
			-- La relation parent-enfant doit �tre d�truite.
			-- Si l'enfant, n'appartenait pas au parent, rien ne doit �tre fait.
		note
			testing:
				"covers/{SCENE_OBJECT}.remove_child",
				"covers/{SCENE_OBJECT}.set_parent"
		local
			l_foreign_parent, l_foreign_child, l_scene_object, l_disrespectful_child: MOCK_SCENE_OBJECT
		do
			create l_scene_object.make
			create l_disrespectful_child.make
			l_scene_object.add_child (l_disrespectful_child)
			l_scene_object.remove_child (l_disrespectful_child)
			assert ("Liens bris�s",
				not l_scene_object.has_child (l_disrespectful_child) and l_disrespectful_child.parent ~ Void)
			create l_foreign_parent.make
			create l_foreign_child.make
			l_foreign_parent.add_child (l_foreign_child)
			l_scene_object.remove_child (l_foreign_child)
			assert ("Pas de kidnaping",
				l_foreign_parent.has_child (l_foreign_child) and l_foreign_child.parent = l_foreign_parent)
		end

	test_has_child_normal
			-- Teste `has_child'.
			-- Doit �tre vrai si le parent poss�de un enfant au premier degr�.
		note
			testing: "covers/{SCENE_OBJECT}.has_child"
		local
			l_parent, l_child: MOCK_SCENE_OBJECT
		do
			create l_parent.make
			create l_child.make
			l_parent.add_child (l_child)
			assert ("l_child est un enfant", l_parent.has_child (l_child))
			l_parent.remove_child (l_child)
			assert ("l_child n'est pas un enfant", not l_parent.has_child (l_child))
		end

	test_subchildren_normal
			-- Teste `subchildren'.
			-- Doit retourner une liste de toute sa descendance.
		note
			testing: "covers/{SCENE_OBJECT}.subchildren"
		local
			l_object1, l_object2, l_object3, l_object4,
			l_object5, l_object6, l_object7: MOCK_SCENE_OBJECT
			l_object1_children: LIST [SCENE_OBJECT]
		do
			create l_object7.make
			create l_object6.make
			l_object6.add_child (l_object7)		--   1
			create l_object5.make				--  / \
			create l_object4.make				-- 2   4
			l_object4.add_child (l_object5)		-- |  / \
			l_object4.add_child (l_object6)		-- 3 5   6
			create l_object3.make				--       |
			create l_object2.make				--       7
			l_object2.add_child (l_object3)
			create l_object1.make
			l_object1.add_child (l_object2)
			l_object1.add_child (l_object4)
			l_object1_children := l_object1.subchildren
			assert ("object1 a 6 descendants", l_object1_children.count ~ 6)
			assert ("object2 pr�sent", l_object1_children.has (l_object2))
			assert ("object3 pr�sent", l_object1_children.has (l_object3))
			assert ("object4 pr�sent", l_object1_children.has (l_object4))
			assert ("object5 pr�sent", l_object1_children.has (l_object5))
			assert ("object6 pr�sent", l_object1_children.has (l_object6))
			assert ("object7 pr�sent", l_object1_children.has (l_object7))
		end

	test_has_subchild_normal
			-- Teste `has_subchild'.
			-- Doit �tre vrai si l'enfant est un descendant du parent.
		note
			testing: "covers/{SCENE_OBJECT}.has_subchild"
		local
			l_parent, l_target_child, l_some_child: MOCK_SCENE_OBJECT
		do
			create l_parent.make
			create l_some_child.make
			l_parent.add_child (l_some_child)
			create l_target_child.make
			l_some_child.add_child (l_target_child)
			assert ("l_target_child est un descendant", l_parent.has_subchild (l_target_child))
			l_some_child.remove_child (l_target_child)
			assert ("l_target_child n'est pas un descendant", not l_parent.has_subchild (l_target_child))
		end

	test_is_subchild_of_normal
			-- Teste `is_subchild_of'.
			-- Doit �tre vrai si un parent est l'anc�tre d'un enfant.
		note
			testing: "covers/{SCENE_OBJECT}.is_subchild_of"
		local
			l_child, l_parent, l_grandparent: MOCK_SCENE_OBJECT
		do
			create l_grandparent.make
			create l_parent.make
			l_grandparent.add_child (l_parent)
			create l_child.make
			l_parent.add_child (l_child)
			assert ("l_parent est un parent de l_child", l_child.is_subchild_of (l_parent))
			assert ("l_grandparent est un parent de l_child", l_child.is_subchild_of (l_grandparent))
			assert ("l_child n'est pas un parent de l_parent", not l_parent.is_subchild_of (l_child))
		end

	test_set_parent_normal
			-- Teste `set_parent'.
		note
			testing:
				"covers/{SCENE_OBJECT}.parent",
				"covers/{SCENE_OBJECT}.set_parent"
		local
			l_parent, l_child: MOCK_SCENE_OBJECT
		do
			create l_parent.make
			create l_child.make
			l_parent.internal_children.extend (l_child)
			l_child.set_parent (l_parent)
			assert ("Parent est assign�", l_child.parent = l_parent)
			l_child.set_parent (Void)
			assert ("Peut assigner un parent Void", l_child.parent = Void)
		end

feature -- Tests cas erron�s

	test_add_child_wrong
			-- Teste `add_child' avec des cas erron�s.
			-- On ne peut pas �tre soi-m�me son propre enfant, ou un parent ne peut pas �tre son enfant.
		note
			testing: "covers/{SCENE_OBJECT}.add_child"
		local
			l_test_number: INTEGER_32
			l_scene_object, l_parent, l_some_child: MOCK_SCENE_OBJECT
		do
			create l_scene_object.make
			if l_test_number ~ 0 then
				l_scene_object.add_child (l_scene_object)
				assert ("Current n'est pas son propre enfant", False)
			elseif l_test_number ~ 1 then
				create l_some_child.make
				l_scene_object.add_child (l_some_child)
				create l_parent.make
				l_parent.add_child (l_scene_object)
				l_some_child.add_child (l_parent)
				assert ("Le parent n'est pas un enfant ou sous-enfant", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name.to_string_8
			then
				l_test_number := l_test_number + 1
				retry
			end
		end

	test_set_parent_wrong
			-- Teste `set_parent' avec un cas erron�.
			-- L'enfant doit �tre ajout� au parent avant d'assigner le parent � l'enfant.
		note
			testing:
				"covers/{SCENE_OBJECT}.parent",
				"covers/{SCENE_OBJECT}.set_parent"
		local
			l_retry: BOOLEAN
			l_parent, l_child: MOCK_SCENE_OBJECT
		do
			if not l_retry then
				create l_parent.make
				create l_child.make
				l_child.set_parent (l_parent)
				assert ("L'enfant doit �tre ajout� au parent avant l'appel de la routine", False)
			end
		rescue
			if
				attached {PRECONDITION_VIOLATION} {EXCEPTIONS}.exception_manager.last_exception as la_exception and then
				la_exception.type_name ~ Current.generating_type.name.to_string_8
			then
				l_retry := True
				retry
			end
		end

end

