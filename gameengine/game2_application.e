note
	description : "Internal Root class of the game.%
				%The content of this file is auto-generated and must not be modified. Every modification will be lost when regenerated."
	generator   : "Eiffel Game2 Project Wizard"
	date        : "2020-11-04 01:19:58.893 +0000"
	revision    : "0.1"
	license     : "CC0"

class
	GAME2_APPLICATION

inherit
	ANY
	ENGINE_SHARED
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	TEXT_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED


create
	make

feature {NONE} -- Initialization

	make
			-- Initialize the libraries and run the game.
		do
			engine.run
			game_library.clear_all_events
			text_library.quit_library
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

end
