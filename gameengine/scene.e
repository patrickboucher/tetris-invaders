﻿note
	description: "Scène du jeu qui contiens les {SCENE_OBJECT}."
	author: "Patrick Boucher"
	date: "10 novembre 2020"
	revision: "1.0"

deferred class
	SCENE

inherit
	SCENE_OBJECT_TREE

feature {NONE} -- Initialization

	make
			-- Initialise `Current'.
		do
			create {LINKED_SET [SCENE_OBJECT]} scene_objects_root.make
		end

	make_and_run
			-- Initialise `Current' avec l'état `Running'.
		deferred
		ensure
			is_running: is_running
		end

feature -- Access

	state: NATURAL
			-- État de la scène

	run
			-- Change l'état de la scène.
			-- Assigne à `state' la valeur de `Running'.
		do
			if state /~ Ended then
				state := Running
			end
		ensure
			ended_state_can_not_change: old state ~ Ended implies state ~ Ended
			running: old state /~ Ended implies state ~ Running
		end

	is_running: BOOLEAN
			-- Indique si la scène est jouable.
		do
			Result := state ~ Running
		end

	pause
			-- Change l'état de la scène.
			-- Assigne à `state' la valeur de `Paused'.
		do
			if state /~ Ended then
				state := Paused
			end
		ensure
			ended_state_can_not_change: old state ~ Ended implies state ~ Ended
			paused: old state /~ Ended implies state ~ Paused
		end

	is_paused: BOOLEAN
			-- Indique si la scène est sur pause.
		do
			Result := state ~ Paused
		end

	Not_yet_started: NATURAL = 0
			-- État où la scène est prête, mais n'a pas commencée.

	Running: NATURAL = 1
			-- État où la scène est jouable.

	Paused: NATURAL = 2
			-- État où la scène est sur pause.
			-- Elle ne doit pas mettre à-jour ses {SCENE_OBJECT}.

	Ended: NATURAL = 3
			-- État où la scène n'est plus jouable et doit être remise à zéro.

feature {ENGINE} -- Updating

	updatables: LIST [SCENE_OBJECT]
			-- Liste de tous les {SCENE_OBJECT} de la scène qui peuvent être mis à-jour.
		local
			l_result: LINKED_LIST [SCENE_OBJECT]
		do
			create {LINKED_SET [SCENE_OBJECT]} Result.make_from_iterable (scene_objects_root)
		end

feature {NONE} -- Implementation

	scene_objects_root: LIST [SCENE_OBJECT]
			-- Racine de tous les {SCENE_OBJECT}. Seule le parent d'un arbre de {SCENE_OBJECT} est nécessaire.
			-- Les éléments de cette liste ainsi que tous leurs enfants seront mis à-jour par {ENGINE} à
			-- chaque itération.

invariant
	unique_scene_object: attached {LINKED_SET [SCENE_OBJECT]} scene_objects_root
	root_only: scene_objects_root.for_all (agent  (item: SCENE_OBJECT): BOOLEAN do Result := not attached item.parent end)

note
	copyright: "Copyright (c) 2020 Patrick Boucher"
	license: "[
		L'autorisation est accordée, gracieusement, à toute personne acquérant une copie
		de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),
		de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,
		de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier
		et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes
		auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :
		La déclaration de copyright ci-dessus et la présente autorisation doivent être
		incluses dans toutes copies ou parties substantielles de la Bibliothèque.
		LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
		EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
		D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN
		AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT
		RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,
		QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN
		PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON
		UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
	]"

end
