﻿note
	description: "Classe qui affiche les images du jeu dans un thread différent."
	author: "Patrick Boucher"
	date: "22 décembre 2020"
	revision: "1.0"

class
	SCENE_RENDERER

inherit
	ENGINE_SHARED
	THREAD
		rename
			make as make_thread
		end

create
	make

feature {NONE} -- Initialization

	make
			-- Initialise `Current' dans un nouveau thread.
		do
			make_thread
			create old_objects_state.make
			create rendering_finished.make
			create rendering_finished_mutex.make
		end

feature -- Access

	set_old_objects_state (a_twins: LIST [SCENE_OBJECT])
			-- Ajoute les éléments de `a_twins' à la fin de la liste à afficher `old_objects_state'.
			-- `a_twins' étant une liste de clone des objets à afficher.
			-- Il est recommandé de passer une {LINKED_LIST} pour plus de performance.
		do
			if attached {LINKED_LIST [SCENE_OBJECT]} a_twins as la_twins then
				old_objects_state.merge_right (la_twins)
			else
				old_objects_state.append (a_twins)
			end
		end

	wait
			-- Attend que `Current' ait fini d'afficher les images.
		do
			rendering_finished_mutex.lock
			waiting_count := waiting_count + 1
			rendering_finished.wait (rendering_finished_mutex)
			waiting_count := waiting_count - 1
			rendering_finished_mutex.unlock
		end

feature {NONE} -- Implementation

	old_objects_state: LINKED_LIST [SCENE_OBJECT]
			-- Une copie des objets à afficher à l'état de leur dernière mise-à-jour.

	execute
			-- Lance le thread.
			-- Affiche les images de `old_objects_state' en boucle jusqu'au signal d'arrêt du jeu.
		do
			from
			until
				Engine.is_quitting
			loop
				if not old_objects_state.is_empty then
					render
				end
				if waiting_count > 0 then
					rendering_finished_mutex.lock
					rendering_finished.signal
					rendering_finished_mutex.unlock
				end
			end
		end

	render
			-- Affiche les images de `old_objects_state'.
		do
			renderer.clear
			old_objects_state.do_all (agent  (item: SCENE_OBJECT)
				do
					if attached {IMAGE} item as la_image then
						renderer.draw_texture (la_image.texture, item.scene_position.x.floor, item.scene_position.y.floor)
					end
				end)
			renderer.present
			old_objects_state.wipe_out
		ensure
			empty_list: old_objects_state.is_empty
		end

	renderer: GAME_RENDERER
			-- Le moteur de rendu de Game2.
		once
			Result := Engine.window.renderer
		end

	rendering_finished: CONDITION_VARIABLE
			-- Variable de condition qui permet aux autres thread d'attendre la fin du rendu.

	rendering_finished_mutex: MUTEX
			-- Mutex de `rendering_finished'.

	waiting_count: INTEGER_32
			-- Nombre de thread qui attendent le signal de `rendering_finished'.

invariant

note
	copyright: "Copyright (c) 2020 Patrick Boucher"
	license: "[
		L'autorisation est accordée, gracieusement, à toute personne acquérant une copie
		de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),
		de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,
		de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier
		et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes
		auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :
		La déclaration de copyright ci-dessus et la présente autorisation doivent être
		incluses dans toutes copies ou parties substantielles de la Bibliothèque.
		LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
		EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
		D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN
		AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT
		RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,
		QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN
		PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON
		UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
	]"

end
