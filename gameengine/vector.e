﻿note
	description: "Un vecteur 2d représentant les coordonnées x et y sur un plan cartésien."
	author: "Patrick Boucher"
	date: "3 novembre 2020"
	revision: "1.0"

class
	VECTOR

create
	default_create,
	make,
	make_with_coordinates

feature -- Initialization

	make
			-- Crée `Current' avec les coordonées (0.0, 0.0).
		do
		end

	make_with_coordinates (a_x, a_y: REAL_64)
			-- Crée `Current' et initialise `x' et `y' avec les valeurs `a_x' et `a_y'.
		do
			x := a_x
			y := a_y
		ensure
			x_is_assigned: x ~ a_x
			y_is_assigned: y ~ a_y
		end

feature -- Access

	x: REAL_64 assign set_x
			-- Position sur l'axe x.

	set_x (a_value: REAL_64)
			-- Assigne `x' avec la valeur `a_value'.
		do
			x := a_value
		ensure
			is_assigned: x ~ a_value
		end

	y: REAL_64 assign set_y
			-- Position sur l'axe y.

	set_y (a_value: REAL_64)
			-- Assigne `y' avec la valeur `a_value'.
		do
			y := a_value
		ensure
			is_assigned: y ~ a_value
		end

	set_coordinates (a_x, a_y: REAL_64)
			-- Assigne `x' et `y' avec les valeurs `a_x' et `a_y'.
		do
			x := a_x
			y := a_y
		ensure
			x_is_assigned: x ~ a_x
			y_is_assigned: y ~ a_y
		end

	set_coordinates_with_vector (a_coordinates: VECTOR)
			-- Assigne `x' et `y' avec les valeurs xy de `a_position'.
		do
			set_coordinates (a_coordinates.x, a_coordinates.y)
		ensure
			is_assigned: Current ~ a_coordinates
			is_copy: Current /= a_coordinates
		end

feature -- Operations

	plus alias "+" (a_other: like Current): like Current
			-- Retourne l'addition de `Current' et `a_other'.
		do
			create Result
			Result.set_x (x + a_other.x)
			Result.set_y (y + a_other.y)
		ensure
			result_valid: Result.x ~ x + a_other.x and Result.y ~ y + a_other.y
		end

	minus alias "-" (a_other: like Current): like Current
			-- Retourne la soustraction de `Current' et `a_other'.
		do
			create Result
			Result.set_x (x - a_other.x)
			Result.set_y (y - a_other.y)
		ensure
			result_valid: Result.x ~ x - a_other.x and Result.y ~ y - a_other.y
		end

feature -- Predefined

	zero: VECTOR
			-- Retourne un nouveau vecteur avec les valeurs (0.0, 0.0).
		do
			create Result.make_with_coordinates (0.0, 0.0)
		ensure
			instance_free: class
			zero_vector: Result.x ~ 0.0 and Result.y ~ 0.0
		end

note
	copyright: "Copyright (c) 2020 Patrick Boucher"
	license: "[
		L'autorisation est accordée, gracieusement, à toute personne acquérant une copie
		de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),
		de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,
		de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier
		et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes
		auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :
		La déclaration de copyright ci-dessus et la présente autorisation doivent être
		incluses dans toutes copies ou parties substantielles de la Bibliothèque.
		LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
		EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
		D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN
		AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT
		RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,
		QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN
		PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON
		UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
	]"

end
