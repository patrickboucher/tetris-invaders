note
	description: "Un {SCENE_OBJECT} qui peut �tre instancier par {EQA_TEST_SET} pour pouvoir tester ses routines."
	author: "Patrick Boucher"
	date: "5 novembre 2020"
	revision: "1.0"

class
	MOCK_SCENE_OBJECT

inherit
	SCENE_OBJECT
		redefine
			set_parent,
			internal_children
		end

create {EQA_TEST_SET}
	make

feature {EQA_TEST_SET} -- Private routines

	set_parent (a_parent: detachable SCENE_OBJECT)
		do
			Precursor (a_parent)
		end

	internal_children: LIST [SCENE_OBJECT]

end
