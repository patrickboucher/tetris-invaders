﻿note
	description: "[
		Objet qui possède un arbre de {SCENE_OBJECT}. Hériter de cette classe pour utiliser {SCENE_OBJECT}.subchildren_no_deuplicate_checking.
		Cette fonction est plus performante que {SCENE_OBJECT}.subchildren, mais elle ne vérifie pas s'il y a des doublons.
	]"
	author: "Patrick Boucher"
	date: "12 novembre 2020"
	revision: "1.0"

deferred class
	SCENE_OBJECT_TREE

note
	copyright: "Copyright (c) 2020 Patrick Boucher"
	license: "[
		L'autorisation est accordée, gracieusement, à toute personne acquérant une copie
		de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),
		de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,
		de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier
		et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes
		auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :
		La déclaration de copyright ci-dessus et la présente autorisation doivent être
		incluses dans toutes copies ou parties substantielles de la Bibliothèque.
		LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
		EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
		D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN
		AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT
		RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,
		QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN
		PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON
		UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
	]"

end
