Un début de jeu qui ne sera probablement jamais complété.
La structure du projet est expérimentale et est basé sur le moteur Unity3D.
Elle sert à démarrer rapidement la création d'un nouveau jeu.
Il sufit de créer des scènes en héritant de la classe {SCENE} et de leur ajouter
des objets de jeu qui héritent des classes {SCENE_OBJECT} et {IMAGE}.
Le moteur se charge automatiquement de mettre à jour tous les objets de jeu qui
ont redéfinis la méthode `update'.

Prérequis

	EiffelStudio et la librairie Eiffel_Game2

Installation

	Windows et Linux

	1. Télécharger et décompresser le projet.
	2. Sous Windows, copier les fichier .dll dans le dossier du jeu,
	   tel qu'indiqué dans les instructions de Eiffel_Game2.
	3. Ouvrir et compiller le jeu dans EiffelStudio.
	4. Lancer le jeu dans EiffelStudio.

Auteur

	Patrick Boucher

Licence
	
	Le projet est sous licence Expat