﻿note
	description: "[
		Le moteur du jeu. Il initialise Eiffel Game2, met à jour les {SCENE} et leurs {SCENE_OBJECT} et dessine les objets à l'écran.
		Seules les scènes avec l'état "Running" sont mises à jour et dessinées. Une fois initialisé, `Current' doit être lancé avec `run'.
	]"
	author: "Patrick Boucher"
	date: "4 novembre 2020"
	revision: "1.0"

class
	ENGINE

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED

create {ENGINE_SHARED}
	make

feature {NONE} -- Initialization

	make
			-- Initialise `Current' et donne la dimension et le titre à la fenêtre du jeu.
		local
			l_window_builder: GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			image_file_library.enable_png
			create l_window_builder
			l_window_builder.set_dimension (1024, 512)
			l_window_builder.set_title ("Tetris Invader")
			l_window_builder.enable_must_renderer_synchronize_update
			window := l_window_builder.generate_window
			create {LINKED_LIST [SCENE]} scenes.make
			create scene_renderer.make
		end

feature -- Access

	create_scenes
			-- Initialise les scènes du jeu.
		do
			scenes.extend (create {PLAY_SCENE}.make_and_run)
		end

	run
			-- Lance le jeu
		do
			create_scenes
			game_library.quit_signal_actions.extend (agent on_quit)
			game_library.iteration_actions.extend (agent on_iteration)
			scene_renderer.launch
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
		end

	on_iteration (a_timestamp: NATURAL_32)
			-- Événement lancé à chaque itération
		local
			l_updatables: LINKED_LIST [SCENE_OBJECT]
			l_freezed_state: LINKED_LIST [SCENE_OBJECT]
		do
			create l_updatables.make
			from
				scenes.start
			until
				scenes.exhausted
			loop
				if scenes.item.is_running then
					l_updatables.append (scenes.item.updatables)
				end
				scenes.forth
			end
			l_freezed_state := l_updatables.deep_twin
			scene_renderer.wait
			scene_renderer.set_old_objects_state (l_freezed_state)
			l_updatables.do_all (agent  (item: SCENE_OBJECT) do item.update end)
		end

	is_quitting: BOOLEAN
			-- Indique si le Game2 a reçu un signal pour quitter l'application.

	window: GAME_WINDOW_RENDERED
			-- La fenêtre du jeu dans laquelle les images sont affichées.

feature {NONE} -- Implementation

	scenes: LIST [SCENE]
			-- Les différentes scènes du jeu.

	scene_renderer: SCENE_RENDERER
			-- Le gestionnaire de rendu exécuté parallèlement.

	on_quit (a_timestamp: NATURAL_32)
			-- Arrête Eiffel Game2.
		do
			is_quitting := True

			game_library.stop
		end

note
	copyright: "Copyright (c) 2020 Patrick Boucher"
	license: "[
		L'autorisation est accordée, gracieusement, à toute personne acquérant une copie
		de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),
		de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,
		de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier
		et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes
		auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :
		La déclaration de copyright ci-dessus et la présente autorisation doivent être
		incluses dans toutes copies ou parties substantielles de la Bibliothèque.
		LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
		EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
		D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN
		AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT
		RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,
		QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN
		PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON
		UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
	]"

end
