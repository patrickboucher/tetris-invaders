﻿note
	description: "Un {SCENE_OBJECT} affichable. L'image doit provenir d'un fichier PNG."
	author: "Patrick Boucher"
	date: "10 novembre 2020"
	revision: "1.0"

deferred class
	IMAGE

inherit
	SCENE_OBJECT
		rename
			make as make_scene_object
		end
	ENGINE_SHARED

feature {NONE} -- Initialisation

	make (a_image_file: STRING)
			-- Initialise `Current' en assignant à `texture'
			-- l'image du fichier qui est à l'adresse `a_image_file'.
			-- Si l'image ne peut être ouverte, `texture' sera représenté par un pixel.
		local
			l_image: IMG_IMAGE_FILE
		do
			make_scene_object
			create l_image.make (a_image_file)
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					create texture.make_from_image (engine.window.renderer, l_image)
				else
					default_texture
				end
			else
				default_texture
			end
		end

	default_texture
			-- Initialise `Current' en assignant à `texture' un format d'un pixel.
		local
			l_pixel_format: GAME_PIXEL_FORMAT
		do
			create l_pixel_format
			l_pixel_format.set_argb8888
			create texture.make (engine.window.renderer, l_pixel_format, 1, 1)
		end

feature -- Access

	texture: GAME_TEXTURE
			-- La texture (image) de `Current'.

invariant

note
	copyright: "Copyright (c) 2020 Patrick Boucher"
	license: "[
		L'autorisation est accordée, gracieusement, à toute personne acquérant une copie
		de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),
		de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,
		de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier
		et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes
		auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :
		La déclaration de copyright ci-dessus et la présente autorisation doivent être
		incluses dans toutes copies ou parties substantielles de la Bibliothèque.
		LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
		EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
		D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN
		AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT
		RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,
		QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN
		PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON
		UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
	]"

end
