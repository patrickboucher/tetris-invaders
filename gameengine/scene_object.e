﻿note
	description: "[
		Classe dont doit hériter chaque objet d'une scène du jeu. Possède des données utiles telles que la position, la rotation et l'échelle d'un objet du jeu.
		Peut être inséré dans les scènes du jeu pour être mis à-jour à chaque itération. Un {SCENE_OBJECT} peut être l'enfant d'un autre {SCENE_OBJECT} et
		peut lui-même posséder des enfants."
	]"
	author: "Patrick Boucher"
	date: "3 novembre"
	revision: "1.0"

deferred class
	SCENE_OBJECT

inherit
	SCENE_OBJECT_TREE

feature -- Initialization

	make
			-- Initialise `Current'.
		do
			create local_position
			create {LINKED_SET [SCENE_OBJECT]} internal_children.make
		end

feature -- Access

	local_position: VECTOR
			-- La position de `Current' par rapport à `parent'.

	set_local_position (a_x, a_y: REAL_64)
			-- Assigne `local_position' avec un {VECTOR} créé depuis les valeurs `a_x' et `a_y'.
		do
			local_position.set_coordinates (a_x, a_y)
		ensure
			is_assigned: local_position.x ~ a_x and local_position.y ~ a_y
		end

	set_local_position_with_vector (a_position: VECTOR)
			-- Assigne `local_position' avec les valeurs xy de `a_position'.
		do
			local_position.set_coordinates_with_vector (a_position)
		ensure
			is_assigned: local_position ~ a_position
		end

	scene_position: VECTOR
			-- La position de `Current' par rapport à la scène.
		do
			if attached parent as la_parent then
				Result := la_parent.scene_position + local_position
			else
				Result := local_position
			end
		end

	set_scene_position (a_x, a_y: REAL_64)
			-- Assigne la position de `Current' dans la scène.
			-- `local_position' est ajusté pour faire correspondre `scene_position'
			-- aux coordonnées (`a_x', `a_u').
		local
			l_position: VECTOR
		do
			create l_position.make_with_coordinates (a_x, a_y)
			set_scene_position_with_vector (l_position)
		ensure
			scene_position_valid: scene_position.x ~ a_x and scene_position.y ~ a_y
		end

	set_scene_position_with_vector (a_position: VECTOR)
			-- Assigne la position de `Current' dans la scène.
			-- `local_position' est ajusté pour faire correspondre `scene_position'
			-- à `a_position'.
		local
			l_local_position: VECTOR
		do
			if attached parent as la_parent then
				l_local_position := a_position - la_parent.scene_position
			else
				l_local_position := a_position
			end
			local_position.set_coordinates_with_vector (l_local_position)
		ensure
			scene_position_valid: scene_position ~ a_position
		end

	parent: detachable SCENE_OBJECT
			-- Le parent de `Current'.

	children: LIST [SCENE_OBJECT]
			-- Les enfants de `Current'
		do
			create {LINKED_LIST [SCENE_OBJECT]} Result.make_from_iterable (internal_children)
		ensure
			is_copy: Result /= internal_children
		end

	add_child (a_child: SCENE_OBJECT)
			-- Définis `a_child' comme étant un enfant de `Current'. Si `a_child' n'est pas orphelin,
			-- il sera retiré de la liste d'enfants de l'ancien parent.
		require
			not_current_or_parent: a_child /= Current and not is_subchild_of (a_child)
		do
			if attached a_child.parent as la_old_parent then
				la_old_parent.remove_child (a_child)
			end
			internal_children.extend (a_child)
			a_child.set_parent (Current)
		ensure
			is_child: has_child (a_child)
			child_parent_is_Current: a_child.parent = Current
		end

	remove_child (a_child: SCENE_OBJECT)
			-- Retire `a_child' de la la liste d'enfants de `Current'.
			-- `a_child' devient orphelin si, et seulement si, il apartenait à `Current'.
		do
			if has_child (a_child) then
				internal_children.prune (a_child)
				a_child.set_parent (Void)
			end
		ensure
			is_not_child: not has_child (a_child)
			child_parent_not_Current: old a_child.parent = Current implies a_child.parent ~ Void
			foreign_child_not_touched: not old Current.has_child (a_child) implies
				a_child.parent = old a_child.parent and
				attached a_child.parent as la_child_parent and then la_child_parent.has_child (a_child)
		end

	has_child (a_child: SCENE_OBJECT): BOOLEAN
			-- Indique si `Current' est le parent de `a_child'.
		do
			Result := internal_children.has (a_child)
		end

	subchildren: LIST [SCENE_OBJECT]
			-- Retourne une liste de tous les descendants de `Current', sans doublons.
		do
			create {LINKED_SET [SCENE_OBJECT]} Result.make_from_iterable (subchildren_no_duplicate_checking)
		end

	has_subchild (a_child: SCENE_OBJECT): BOOLEAN
			-- Indique si `a_child' est un descendant de `Current'.
		do
			from
				internal_children.start
			until
				internal_children.exhausted or Result ~ True
			loop
				if internal_children.item = a_child then
					Result := True
				else
					Result := internal_children.item.has_subchild (a_child)
				end
				internal_children.forth
			end
		end

	is_subchild_of (a_object: SCENE_OBJECT): BOOLEAN
			-- Indique si `Current' est un descendant de `a_parent'.
		local
			l_parent: SCENE_OBJECT
		do
			from
				l_parent := parent
			until
				l_parent = Void or Result ~ True
			loop
				if l_parent = a_object then
					Result := True
				elseif attached l_parent as la_parent then
					l_parent := la_parent.parent
				end
			end
		end

feature {ENGINE} -- Updating

	update
			-- Lancé à chaque itération. `Current' ou un parent doit être inclus dans la liste de mise-à-jour
			-- des {SCENE_OBJECT} d'un objet {SCENE}.
			-- Redéfinir cette méthode pour "donner vie" à un {SCENE_OBJECT}.
		do
		end

feature {SCENE_OBJECT} -- Implementation

	set_parent (a_parent: detachable SCENE_OBJECT)
			-- Assigne `parent' avec `a_parent'.
			-- Cette routine doit être exécutée après avoir ajouté `Current' à un parent.
		require
			is_parent: attached a_parent implies a_parent.has_child (Current)
		do
			parent := a_parent
		ensure
			is_assigned: parent = a_parent
		end

feature {SCENE_OBJECT_TREE} -- Implementation

	subchildren_no_duplicate_checking: LIST [SCENE_OBJECT]
			-- Retourne une liste de tous les descendants de `Current'.
			-- Pour une question de performance, les doublons ne sont pas vérifiés pendant cette étape.
		local
			l_result: LINKED_LIST [SCENE_OBJECT]
		do
			create l_result.make
			across internal_children as la_children loop
				l_result.extend (la_children.item)
				if attached {LINKED_LIST [SCENE_OBJECT]} la_children.item.subchildren_no_duplicate_checking as la_subchildren then
					l_result.merge_right (la_subchildren)
				end
			end
			Result := l_result
		end

feature {NONE} -- Implementation

	internal_children: LIST [SCENE_OBJECT]
			-- Liste des enfants de `Current'.

invariant
	unique_children: attached {LINKED_SET [SCENE_OBJECT]} internal_children
	avoid_infernal_loop: attached parent as la_parent implies not has_subchild (la_parent)
	avoid_weird_relationchip: not has_subchild (Current) and attached parent as la_parent implies not is_subchild_of (Current)

note
	copyright: "Copyright (c) 2020 Patrick Boucher"
	license: "[
		L'autorisation est accordée, gracieusement, à toute personne acquérant une copie
		de cette bibliothèque et des fichiers de documentation associés (la "Bibliothèque"),
		de commercialiser la Bibliothèque sans restriction, notamment les droits d'utiliser,
		de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier
		et / ou de vendre des copies de la Bibliothèque, ainsi que d'autoriser les personnes
		auxquelles la Bibliothèque est fournie à le faire, sous réserve des conditions suivantes :
		La déclaration de copyright ci-dessus et la présente autorisation doivent être
		incluses dans toutes copies ou parties substantielles de la Bibliothèque.
		LA BIBLIOTHÈQUE EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE,
		EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE,
		D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN
		AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT
		RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ,
		QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN
		PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LA BIBLIOTHÈQUE OU SON
		UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DE LA BIBLIOTHÈQUE.
	]"

end
